﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UpBroadcast
{
    class VlcService
    {
        internal void Start()
        {
            Console.WriteLine("Starting vlc media player service");
            StartVlcService();
        }

        internal void Stop()
        {
            Console.WriteLine("Stopping vlc media player service");
        }

        private void StartVlcService()
        {
            System.Diagnostics.Process.Start("vlc");
        }
    }
}
