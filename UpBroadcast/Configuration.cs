﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topshelf;

namespace UpBroadcast
{
    public static class Configuration
    {
        public static void Configure()
        {
            HostFactory.Run(c =>
            {
                c.Service<VlcService>(service =>
                {
                    service.ConstructUsing(s => new VlcService());
                    service.WhenStarted(s => s.Start());
                    service.WhenStopped(s => s.Stop());  
                });
                c.RunAsLocalSystem();
                c.StartAutomatically();
                c.SetServiceName("UpBroadcast");
                c.SetDisplayName("UpBroadcast");
                c.SetDescription("UpBroadcast service to broad cast videos");
            });
        }
    }
}
